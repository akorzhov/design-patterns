package com.intervale.courses.java.designpatterns.structural.proxy.example1;

/**
 * "Subject"
 */
public interface IMath {

    double add(double x, double y);

    double sub(double x, double y);

    double mul(double x, double y);

    double div(double x, double y);
}
