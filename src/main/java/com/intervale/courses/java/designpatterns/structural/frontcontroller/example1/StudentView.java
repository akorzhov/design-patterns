package com.intervale.courses.java.designpatterns.structural.frontcontroller.example1;

public class StudentView {
   public void show(){
      System.out.println("Displaying Student Page");
   }
}