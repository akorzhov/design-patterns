package com.intervale.courses.java.designpatterns.behavioral.chainofresponsibility.example1;

class EmailLogger extends Logger {
    public EmailLogger(int mask) {
        this.mask = mask;
    }

    protected void writeMessage(String msg) {
        System.out.println("Sending via email: " + msg);
    }
}