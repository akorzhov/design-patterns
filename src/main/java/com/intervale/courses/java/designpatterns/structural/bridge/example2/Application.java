package com.intervale.courses.java.designpatterns.structural.bridge.example2;

public class Application {

    public static void main(String[] args) {
        //a square with red color
        Shape square = new Square(new Red());
        System.out.println("Square drawn. Color is Red".equals(square.draw()));
    }

}
