package com.intervale.courses.java.designpatterns.behavioral.templatemethod.example1;/*	Коды разновидностей игр.
 *
 *      Файл GameCode.java
 * */

public enum GameCode {
    CHESS,
    MONOPOLY
}