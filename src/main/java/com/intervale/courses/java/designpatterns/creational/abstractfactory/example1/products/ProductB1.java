package com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.products;

public class ProductB1 implements AbstractProductB {

    @Override
    public void interact(AbstractProductA a) {
        System.out.println(this.getClass().getName() + " interacts with " + a.getClass().getName());
    }

}