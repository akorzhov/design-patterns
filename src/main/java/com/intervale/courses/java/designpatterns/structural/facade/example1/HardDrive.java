package com.intervale.courses.java.designpatterns.structural.facade.example1;

class HardDrive {
    public byte[] read(long lba, int size) {
        return new byte[0];
    }
}