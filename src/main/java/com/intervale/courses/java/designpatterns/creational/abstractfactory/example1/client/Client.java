package com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.client;

import com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.factory.AbstractFactory;
import com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.products.AbstractProductA;
import com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.products.AbstractProductB;

public class Client {
    private AbstractProductA productA;
    private AbstractProductB productB;

    public Client(AbstractFactory factory) {
        productA = factory.createProductA();
        productB = factory.createProductB();
    }

    public void execute() {
        productB.interact(productA);
    }
}