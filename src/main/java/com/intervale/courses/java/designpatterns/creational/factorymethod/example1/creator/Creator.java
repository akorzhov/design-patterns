package com.intervale.courses.java.designpatterns.creational.factorymethod.example1.creator;

import com.intervale.courses.java.designpatterns.creational.factorymethod.example1.product.Product;

public abstract class Creator {
    public abstract Product factoryMethod();
}