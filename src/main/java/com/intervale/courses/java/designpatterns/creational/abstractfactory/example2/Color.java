package com.intervale.courses.java.designpatterns.creational.abstractfactory.example2;

public interface Color {
    String getRed();
    String getGreen();
    String getBlue();
}
