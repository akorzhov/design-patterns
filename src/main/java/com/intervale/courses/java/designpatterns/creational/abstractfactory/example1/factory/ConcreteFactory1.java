package com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.factory;

import com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.products.AbstractProductA;
import com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.products.AbstractProductB;
import com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.products.ProductA1;
import com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.products.ProductB1;

public class ConcreteFactory1 implements AbstractFactory {

    @Override
    public AbstractProductA createProductA() {
        return new ProductA1();
    }

    @Override
    public AbstractProductB createProductB() {
        return new ProductB1();
    }
}