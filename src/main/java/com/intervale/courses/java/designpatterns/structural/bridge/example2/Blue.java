package com.intervale.courses.java.designpatterns.structural.bridge.example2;

public class Blue implements Color {
    @Override
    public String fill() {
        return "Color is Blue";
    }
}