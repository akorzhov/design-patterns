package com.intervale.courses.java.designpatterns.structural.decorator.example1;

class MainComponent implements InterfaceComponent {

    @Override
    public void doOperation() {
        System.out.print("World!");
    }
}