package com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.products;

public interface AbstractProductA {
    void interact(AbstractProductB b);
}