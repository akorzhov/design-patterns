package com.intervale.courses.java.designpatterns.creational.factorymethod.example2;

public class Heptagon implements Polygon {
    @Override
    public String getType() {
        return "Heptagon";
    }
}
