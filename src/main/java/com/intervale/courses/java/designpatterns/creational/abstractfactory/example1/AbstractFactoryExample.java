package com.intervale.courses.java.designpatterns.creational.abstractfactory.example1;

import com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.client.Client;
import com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.factory.AbstractFactory;
import com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.factory.ConcreteFactory1;
import com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.factory.ConcreteFactory2;

public class AbstractFactoryExample {

    public static void main(String[] args) {

        AbstractFactory factory1 = new ConcreteFactory1();
        Client client1 = new Client(factory1);
        client1.execute();

        AbstractFactory factory2 = new ConcreteFactory2();
        Client client2 = new Client(factory2);
        client2.execute();
    }
}