package com.intervale.courses.java.designpatterns.structural.bridge.example1;

public interface Drawer {

    void drawCircle(int x, int y, int radius);

}