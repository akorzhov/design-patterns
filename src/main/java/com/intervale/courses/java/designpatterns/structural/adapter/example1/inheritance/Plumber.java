package com.intervale.courses.java.designpatterns.structural.adapter.example1.inheritance;

// Adaptee
public class Plumber {
    public Object getScrewNut() {
        System.out.println("Get screw");
        return new Object();
    }

    public Object getPipe() {
        System.out.println("get pipe");
        return new Object();
    }

    public Object getGasket() {
        System.out.println("get gasket");
        return new Object();
    }
}