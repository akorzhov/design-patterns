package com.intervale.courses.java.designpatterns.structural.bridge.example2;

public class Square extends Shape {
 
    public Square(Color color) {
        super(color);
    }
 
    @Override
    public String draw() {
        return "Square drawn. " + color.fill();
    }
}