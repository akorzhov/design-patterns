package com.intervale.courses.java.designpatterns.creational.multiton.example1;

import java.util.HashMap;
import java.util.Map;

public class StudentDatabase {
    private static Map<Student, Address> studentDb = new HashMap<Student, Address>();

    /* Get the Address associated with student */
    public static Address getStudentAddress(Student s) {
        /* Check whether student exist in database or not */
        if (studentDb.containsKey(s)) {
            Address addr;
            addr = studentDb.get(s);

            /* If address not exist for this student, create address instance and map it to student */
            if (addr == null) {
                addr = new Address();
                studentDb.put(s, addr);
            }
            return addr;
        }
        return null;
    }

    public static Map<Student, Address> getStudentDb() {
        return studentDb;
    }

    public static void setStudentDb(Map<Student, Address> studentDb) {
        StudentDatabase.studentDb = studentDb;
    }
}