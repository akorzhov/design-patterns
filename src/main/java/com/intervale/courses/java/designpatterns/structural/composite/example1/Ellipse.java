package com.intervale.courses.java.designpatterns.structural.composite.example1;

/**
 * "Leaf"
 */
class Ellipse implements Graphic {

    //Prints the graphic.
    public void print() {
        System.out.println("Ellipse");
    }

}