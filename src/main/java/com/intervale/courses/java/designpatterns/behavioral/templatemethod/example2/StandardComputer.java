package com.intervale.courses.java.designpatterns.behavioral.templatemethod.example2;

import java.util.Map;

public class StandardComputer extends Computer {

    public StandardComputer(Map<String, String> computerParts) {
        super(computerParts);
    }
}