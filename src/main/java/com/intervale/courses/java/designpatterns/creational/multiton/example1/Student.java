package com.intervale.courses.java.designpatterns.creational.multiton.example1;

public class Student {
    private String firstName;
    private String lastName;
    private int age;
    private int id;

    public int getAge() {
        return age;
    }

    public String getFirstName() {
        return firstName;
    }

    public int getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}