package com.intervale.courses.java.designpatterns.behavioral.state.example1;

interface State {
    String getName();

    void freeze(StateContext context);

    void heat(StateContext context);
}