package com.intervale.courses.java.designpatterns.behavioral.command.example1;

/**
 * The Command interface
 */
interface Command {
    void execute();
}