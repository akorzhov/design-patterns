package com.intervale.courses.java.designpatterns.creational.raii.example1;

public class LogFile implements AutoCloseable {

    private String name;

    public LogFile(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void write(String s) {
        System.out.println(name + ": " + s);
    }

    @Override
    public void close() {
        System.out.println("closed");
    }
}
