package com.intervale.courses.java.designpatterns.behavioral.visitor.example2;

public interface Visitor {

    void visit(XmlElement xe);

    void visit(JsonElement je);
}