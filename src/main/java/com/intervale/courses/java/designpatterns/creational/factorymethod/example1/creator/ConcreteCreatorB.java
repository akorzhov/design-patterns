package com.intervale.courses.java.designpatterns.creational.factorymethod.example1.creator;

import com.intervale.courses.java.designpatterns.creational.factorymethod.example1.product.ConcreteProductB;
import com.intervale.courses.java.designpatterns.creational.factorymethod.example1.product.Product;

public class ConcreteCreatorB extends Creator {
    @Override
    public Product factoryMethod() {
        return new ConcreteProductB();
    }
}