package com.intervale.courses.java.designpatterns.structural.decorator.example2;

public class Application {

    public static void main(String[] args) {
        ChristmasTree tree1 = new Garland(new ChristmasTreeImpl());
        System.out.println("Christmas tree with Garland".equals(tree1.decorate()));

        ChristmasTree tree2 = new BubbleLights(
                new Garland(new Garland(new ChristmasTreeImpl())));
        System.out.println("Christmas tree with Garland with Garland with Bubble Lights".equals(
                tree2.decorate()));
    }
}
