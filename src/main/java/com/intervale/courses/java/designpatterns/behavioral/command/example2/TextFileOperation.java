package com.intervale.courses.java.designpatterns.behavioral.command.example2;

@FunctionalInterface
public interface TextFileOperation {
    String execute();
}
