package com.intervale.courses.java.designpatterns.creational.abstractfactory.example2;

public interface Animal {
    String getAnimal();

    String makeSound();
}