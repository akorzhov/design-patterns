package com.intervale.courses.java.designpatterns.behavioral.mediator.example2;

public class PowerSupplier {
    public void turnOn() {
        // implementation
    }

    public void turnOff() {
        // implementation
    }
}