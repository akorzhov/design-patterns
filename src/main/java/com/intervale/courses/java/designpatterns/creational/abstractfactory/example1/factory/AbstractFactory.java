package com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.factory;

import com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.products.AbstractProductA;
import com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.products.AbstractProductB;

public interface AbstractFactory {
    AbstractProductA createProductA();

    AbstractProductB createProductB();
}