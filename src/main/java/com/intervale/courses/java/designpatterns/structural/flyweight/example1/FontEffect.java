package com.intervale.courses.java.designpatterns.structural.flyweight.example1;

public enum FontEffect {
    BOLD, ITALIC, SUPERSCRIPT, SUBSCRIPT, STRIKETHROUGH
}