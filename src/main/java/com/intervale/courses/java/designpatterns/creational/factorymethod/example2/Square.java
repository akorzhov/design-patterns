package com.intervale.courses.java.designpatterns.creational.factorymethod.example2;

public class Square implements Polygon {
    @Override
    public String getType() {
        return "Square";
    }
}
