package com.intervale.courses.java.designpatterns.behavioral.templatemethod.example2;

import java.util.Map;

public class HighEndComputer extends Computer {

    public HighEndComputer(Map<String, String> computerParts) {
        super(computerParts);
    }
}