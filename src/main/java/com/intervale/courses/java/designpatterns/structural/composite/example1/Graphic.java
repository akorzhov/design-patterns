package com.intervale.courses.java.designpatterns.structural.composite.example1;

/**
 * "Component"
 */
interface Graphic {

    //Prints the graphic.
    public void print();

}