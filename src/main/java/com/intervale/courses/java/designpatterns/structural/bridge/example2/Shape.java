package com.intervale.courses.java.designpatterns.structural.bridge.example2;

public abstract class Shape {
    protected Color color;

    public Shape(Color color) {
        this.color = color;
    }

    abstract public String draw();
}