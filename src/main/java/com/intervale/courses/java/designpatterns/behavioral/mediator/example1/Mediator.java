package com.intervale.courses.java.designpatterns.behavioral.mediator.example1;

public abstract class Mediator {

    public abstract void send(String message, Colleague sender);
}