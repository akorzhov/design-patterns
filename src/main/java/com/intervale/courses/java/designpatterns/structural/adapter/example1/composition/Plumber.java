package com.intervale.courses.java.designpatterns.structural.adapter.example1.composition;// Файл Plumber.java

public class Plumber {

    public Object getPipe() {
        return new Object();
    }

    public Object getKey() {
        return new Object();
    }

    public Object getScrewDriver() {
        return new Object();
    }

}
