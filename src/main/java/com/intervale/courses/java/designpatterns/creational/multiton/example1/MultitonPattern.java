package com.intervale.courses.java.designpatterns.creational.multiton.example1;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class MultitonPattern {
    public static void main(String[] args) {
        Map<Student, Address> studentDb = new HashMap<Student, Address>();

        Student s1 = new Student();
        Address a1 = null;

        s1.setFirstName("Krishna");
        s1.setLastName("Gurram");

        studentDb.put(s1, a1);

        StudentDatabase.setStudentDb(studentDb);

        Address addr = StudentDatabase.getStudentAddress(s1);
        Objects.requireNonNull(addr);
        addr.setCity("Ongole");
        addr.setCountry("India");

        addr = StudentDatabase.getStudentAddress(s1);
        Objects.requireNonNull(addr);
        System.out.println(addr.getCity());
        System.out.println(addr.getCountry());

    }
}
