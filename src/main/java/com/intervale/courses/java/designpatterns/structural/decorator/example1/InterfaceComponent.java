package com.intervale.courses.java.designpatterns.structural.decorator.example1;

public interface InterfaceComponent {
    void doOperation();
}