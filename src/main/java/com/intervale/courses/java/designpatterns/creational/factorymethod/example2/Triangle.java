package com.intervale.courses.java.designpatterns.creational.factorymethod.example2;

public class Triangle implements Polygon {
    @Override
    public String getType() {
        return "Triangle";
    }
}
