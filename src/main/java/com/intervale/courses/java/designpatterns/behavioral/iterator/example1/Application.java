package com.intervale.courses.java.designpatterns.behavioral.iterator.example1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Application {

    public static void main(String[] args) {
        List<Notification> notificationList = new ArrayList<>();
        notificationList.add(new Notification());
        // Create an iterator
        Iterator<Notification> iterator = notificationList.iterator();

        // It wouldn’t matter if list is Array or ArrayList or
        // anything else.
        while (iterator.hasNext()) {
            Notification notification = iterator.next();
        }

    }
}
