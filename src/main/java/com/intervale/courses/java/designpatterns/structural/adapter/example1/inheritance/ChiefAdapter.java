package com.intervale.courses.java.designpatterns.structural.adapter.example1.inheritance;

// Adapter
public class ChiefAdapter extends Plumber implements Chief {
    public Object makeBreakfast() {
        return getGasket();
    }

    public Object makeLunch() {
        return getPipe();
    }

    public Object makeDinner() {
        return getScrewNut();
    }
}