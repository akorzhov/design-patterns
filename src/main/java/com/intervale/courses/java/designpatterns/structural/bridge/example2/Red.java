package com.intervale.courses.java.designpatterns.structural.bridge.example2;

public class Red implements Color {
    @Override
    public String fill() {
        return "Color is Red";
    }
}
