package com.intervale.courses.java.designpatterns.structural.flyweight.example2;

public abstract class EnglishCharacter {

	protected char symbol;
	
	protected int width;
	
	protected int height;
	
	public abstract void printCharacter();
	
}