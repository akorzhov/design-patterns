package com.intervale.courses.java.designpatterns.structural.facade.example1;/* Complex parts */

class CPU {
    public void freeze() {
    }

    public void jump(long position) {
    }

    public void execute() {
    }
}