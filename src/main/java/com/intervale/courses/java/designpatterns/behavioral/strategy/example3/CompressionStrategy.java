package com.intervale.courses.java.designpatterns.behavioral.strategy.example3;

import java.io.File;
import java.util.ArrayList;

public interface CompressionStrategy {
    public void compressFiles(ArrayList<File> files);
}
