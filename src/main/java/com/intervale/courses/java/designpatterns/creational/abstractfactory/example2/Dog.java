package com.intervale.courses.java.designpatterns.creational.abstractfactory.example2;

public class Dog implements Animal {
    @Override
    public String getAnimal() {
        return "Dog";
    }

    @Override
    public String makeSound() {
        return "woof";
    }
}
