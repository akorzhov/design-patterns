package com.intervale.courses.java.designpatterns.creational.raii.example1;

public class RaiiExample {

    public static void main(String[] args) {
        // открываем файл (захватываем ресурс)
        final LogFile logfile = new LogFile("logfile.txt");

        try {
            logfile.write("hello logfile!");

            // продолжаем использовать logfile...
            // Можно возбуждать исключения, не беспокоясь о закрытии файла.
            // Файл будет закрыт при выполнении блока finally, который
            // гарантированно выполняется после блока try даже в случае
            // возникновения исключений.
        } finally {
            // явно освобождаем ресурс
            logfile.close();
        }
    }
}
