package com.intervale.courses.java.designpatterns.behavioral.interpreter.example1;

import java.util.List;

interface Expression {
    List<String> interpret(Context ctx);
}