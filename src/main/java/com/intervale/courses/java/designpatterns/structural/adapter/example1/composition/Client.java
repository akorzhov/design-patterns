package com.intervale.courses.java.designpatterns.structural.adapter.example1.composition;// Файл Client.java

public class Client {

    public static void main(String[] args) {
        Chief chief = new ChiefAdapter();

        Object key = chief.makeDinner();
    }

}