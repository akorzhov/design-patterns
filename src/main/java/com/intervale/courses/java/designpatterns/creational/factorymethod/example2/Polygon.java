package com.intervale.courses.java.designpatterns.creational.factorymethod.example2;

public interface Polygon {
    String getType();
}