package com.intervale.courses.java.designpatterns.creational.factorymethod.example1;

import com.intervale.courses.java.designpatterns.creational.factorymethod.example1.creator.ConcreteCreatorA;
import com.intervale.courses.java.designpatterns.creational.factorymethod.example1.creator.ConcreteCreatorB;
import com.intervale.courses.java.designpatterns.creational.factorymethod.example1.creator.Creator;
import com.intervale.courses.java.designpatterns.creational.factorymethod.example1.product.Product;

public class FactoryMethodExample {

    public static void main(String[] args) {
        // an array of creators
        Creator[] creators = {new ConcreteCreatorA(), new ConcreteCreatorB()};
        // iterate over creators and create products
        for (Creator creator : creators) {
            Product product = creator.factoryMethod();
            System.out.printf("Created {%s}\n", product.getClass());
        }
    }
}