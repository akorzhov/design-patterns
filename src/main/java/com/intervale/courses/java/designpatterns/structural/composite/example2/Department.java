package com.intervale.courses.java.designpatterns.structural.composite.example2;

public interface Department {
    void printDepartmentName();
}