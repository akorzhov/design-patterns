package com.intervale.courses.java.designpatterns.creational.factorymethod.example1.creator;

import com.intervale.courses.java.designpatterns.creational.factorymethod.example1.product.ConcreteProductA;
import com.intervale.courses.java.designpatterns.creational.factorymethod.example1.product.Product;

public class ConcreteCreatorA extends Creator {
    @Override
    public Product factoryMethod() { return new ConcreteProductA(); }
}
