package com.intervale.courses.java.designpatterns.structural.adapter.example1.inheritance;

// Target
public interface Chief {
    public Object makeBreakfast();

    public Object makeLunch();

    public Object makeDinner();
}