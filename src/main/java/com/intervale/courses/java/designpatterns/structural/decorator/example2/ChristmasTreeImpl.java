package com.intervale.courses.java.designpatterns.structural.decorator.example2;

public class ChristmasTreeImpl implements ChristmasTree {
 
    @Override
    public String decorate() {
        return "Christmas tree";
    }
}