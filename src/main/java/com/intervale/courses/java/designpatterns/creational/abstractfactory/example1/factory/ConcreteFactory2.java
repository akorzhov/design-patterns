package com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.factory;

import com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.products.AbstractProductA;
import com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.products.AbstractProductB;
import com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.products.ProductA2;
import com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.products.ProductB2;

public class ConcreteFactory2 implements AbstractFactory {

    @Override
    public AbstractProductA createProductA() {
        return new ProductA2();
    }

    @Override
    public AbstractProductB createProductB() {
        return new ProductB2();
    }
}