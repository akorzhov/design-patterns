package com.intervale.courses.java.designpatterns.creational.lazyinitialization.example1;

public class LazyInit {
    private static Resource resource;

    public static Resource getResource() {
        if (resource == null) {
            synchronized (LazyInit.class) {
                if (resource == null) {
                    resource = new Resource();
                }
            }
        }
        return resource;
    }
}
