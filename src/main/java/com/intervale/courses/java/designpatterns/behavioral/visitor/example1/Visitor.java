package com.intervale.courses.java.designpatterns.behavioral.visitor.example1;

interface Visitor {
	public void visit ( Point2d p );
	public void visit ( Point3d p );
}