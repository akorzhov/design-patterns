package com.intervale.courses.java.designpatterns.structural.bridge.example2;

public interface Color {
    String fill();
}