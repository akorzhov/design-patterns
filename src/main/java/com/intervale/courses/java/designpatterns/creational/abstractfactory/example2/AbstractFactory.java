package com.intervale.courses.java.designpatterns.creational.abstractfactory.example2;

public interface AbstractFactory<T> {
    T create(String type) ;
}