package com.intervale.courses.java.designpatterns.creational.factorymethod.example2;

public class Octagon implements Polygon {
    @Override
    public String getType() {
        return "Octagon";
    }
}
