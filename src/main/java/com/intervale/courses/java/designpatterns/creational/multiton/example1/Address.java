package com.intervale.courses.java.designpatterns.creational.multiton.example1;

public class Address {
    private String street;
    private String city;
    private String district;
    private String state;
    private String country;
    private String PIN;

    public String getPIN() {
        return PIN;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getDistrict() {
        return district;
    }

    public String getState() {
        return state;
    }

    public String getStreet() {
        return street;
    }

    public void setPIN(String PIN) {
        this.PIN = PIN;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}