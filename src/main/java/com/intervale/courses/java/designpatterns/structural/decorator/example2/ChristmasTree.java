package com.intervale.courses.java.designpatterns.structural.decorator.example2;

public interface ChristmasTree {
    String decorate();
}