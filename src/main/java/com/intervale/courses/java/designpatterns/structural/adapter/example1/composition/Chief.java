package com.intervale.courses.java.designpatterns.structural.adapter.example1.composition;// Файл Chief.java

public interface Chief {

    public Object makeBreakfast();

    public Object makeDinner();

    public Object makeSupper();

}