package com.intervale.courses.java.designpatterns.behavioral.command.example1;

/**
 * The Receiver class
 */
class Light {
    public void turnOn() {
        System.out.println("The light is on");
    }

    public void turnOff() {
        System.out.println("The light is off");
    }
}