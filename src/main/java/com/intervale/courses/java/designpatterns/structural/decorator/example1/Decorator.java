package com.intervale.courses.java.designpatterns.structural.decorator.example1;

abstract class Decorator implements InterfaceComponent {
    protected InterfaceComponent component;

    public Decorator(InterfaceComponent c) {
        component = c;
    }

    @Override
    public void doOperation() {
        component.doOperation();
    }

    public void newOperation() {
        System.out.println("Do Nothing");
    }
}