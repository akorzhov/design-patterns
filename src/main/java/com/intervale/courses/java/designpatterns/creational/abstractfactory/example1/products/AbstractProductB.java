package com.intervale.courses.java.designpatterns.creational.abstractfactory.example1.products;

public interface AbstractProductB {
    void interact(AbstractProductA a);
}