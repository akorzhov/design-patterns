package com.intervale.courses.java.designpatterns.structural.composite.example2;

public class FinancialDepartment implements Department {

    private Integer id;
    private String name;

    public FinancialDepartment(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void printDepartmentName() {
        System.out.println(getClass().getSimpleName());
    }
}