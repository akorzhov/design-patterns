package com.intervale.courses.java.designpatterns.creational.abstractfactory.example2;

public class Duck implements Animal {
 
    @Override
    public String getAnimal() {
        return "Duck";
    }
 
    @Override
    public String makeSound() {
        return "Squeks";
    }
}