package com.intervale.courses.java.designpatterns.structural.frontcontroller.example1;

public class HomeView {
    public void show() {
        System.out.println("Displaying Home Page");
    }
}