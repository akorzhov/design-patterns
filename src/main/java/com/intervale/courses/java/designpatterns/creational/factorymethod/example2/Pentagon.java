package com.intervale.courses.java.designpatterns.creational.factorymethod.example2;

public class Pentagon implements Polygon {
    @Override
    public String getType() {
        return "Pentagon";
    }
}
